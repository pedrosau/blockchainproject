#!/usr/bin/env python3

import threading
from Directory import DirectoryEntry, Directory
from Peer import Peer
from NetworkQueue import NetworkQueue
import time
from datetime import datetime
from queue import Queue
from TransactionBlock import TransactionBlock, Transaction, TranType
from WebsiteBlock import *

import hashlib
import os
import shutil

# TODO
'''
	- main-to-node connection: internal queue? 
	- seed public keys: a function to randomly pick an index of array?
'''

class Node_Process(threading.Thread):

	def __init__(self, network, ID, public_key, private_key, instructions_queue, initial_neighbor=None):

		super().__init__()

		self.network = network
		self.ID = ID
		self.public_key = public_key
		self.private_key = private_key
		self.instructions_queue = instructions_queue

		self.continue_running = True
		self.folder_name = ""
		self.is_mining = True

		self.neighbors = []
		self.blockchain = []
		self.directory = Directory()
		self.hosted_sites = []

		if initial_neighbor != None: 
			p = Peer(initial_neighbor)
			self.neighbors.append(p)
			self.contact_new_peer(p)
		else:
			initial_block = TransactionBlock(index="0",
				timestamp=datetime.utcnow().timestamp(),
				data=Transaction(recipient=self.public_key, amount="50", type=TranType.INITIALIZATION),
				previous_block_hash="0".zfill(64),
				directory_hash=self.directory.hash())
			initial_block.mine()
			self.blockchain.append(initial_block)

	def run(self):
		# # Create a file directory for web data
		self.create_file_directory()

		# Loop over queues in handler
		self.handler() 

	# Handle requests in the queue
	def handler(self):

		while self.continue_running == True:

			time.sleep(1)

			if len(self.blockchain) == 0:
				self.initial_block_download()

			# Check Instruction Queue
			if self.instructions_queue.empty() == False:
				self.instructions_handler(self.instructions_queue.get())

			# Check Network Queue
			if self.network[self.public_key].empty() == True:
				continue				
			
			msg = self.network[self.public_key].receive_message(self.private_key)	# msg will always be a list

			sender = msg[1]
			msg_type = msg[0][0]
			msg = msg[0][1:]

			print("ID: {},\tmsg received: {}".format(self.ID, msg_type))

			if msg_type == "contact_request":
				self.new_peer_request(msg, sender)
			elif msg_type == "request_blockchain_download":
				self.send_blockchain(msg, sender)
			elif msg_type == "blockchain_upload":
				self.receive_blockchain(msg, sender)
			elif msg_type == "new_published_site":
				self.handle_new_site(msg, sender)
			# elif msg_type == "exiting_peer":
			# 	self.remove_neighbor(msg)
			# elif msg_type == "maintain_connection":
			# 	self.return_maintain_connection(msg)
			elif msg_type == "exiting_node":
				self.neighbor_exiting(msg, sender)




	''' 
	Initiate Conversation with Peer
	'''
	# Initial contact to a future neighbor: message format = [message type,timestamp]
	def contact_new_peer(self, new_peer):
		msg = ["contact_request", datetime.utcnow().timestamp()]
		# print("contact_new_peer: {}".format(msg))
		try:
			self.network[new_peer.public_key].send_message(msg, self.public_key)
		except Exception as e:
			print("network_connections did not work: \t{}".format(e))
		# print("debug: post send_message()")

	# New node receives blockchain from peer
	def initial_block_download(self):
		if len(self.neighbors) == 0:
			return
		msg = ["request_blockchain_download", datetime.utcnow().timestamp()]
		self.network[self.neighbors[0].public_key].send_message(msg, self.public_key)

	def announce_published_site(self, dir_update):
		msg = ["new_published_site", dir_update, datetime.utcnow().timestamp()]
		for peer in self.neighbors:
			self.network[peer.public_key].send_message([msg, self.public_key])

	# def get_block(self):
	# 	# get a block from neighbor

	# def request_site(self, url):
	# 	# request website data from peers (function called from main)

	# def broadcast_block(self):
	# 	# broadcast mined block to neighbors

	# def maintain_connection(self, peer):
	# 	# send a message to silent neighbors
	#	msg = ["maintain_connection", self.ID, timestamp]
	#	peer.queue.put(msg)




	'''
	Receive Message & Respond to Peer
	'''
	# Send entire blockchain to peer requesting Initial Block Download
	def send_blockchain(self, msg, sender):
		my_array = self.blockchain
		msg = ["blockchain_upload", my_array, datetime.utcnow().timestamp()]
		self.network[sender].send_message(msg, self.public_key)

	# def return_maintain_connection(self,msg):
	#	# find peer by ID
	#	msg = ["connected"]
	#	peer.queue.put[msg]




	'''
	Receive message from peer (with no response)
	'''
	# an unknown node wants to become neighbors; does not respond
	def new_peer_request(self, msg, sender):
		# incoming message format: [peer's queue, peer's ID, timestamp]
		p = Peer(sender)
		self.neighbors.append(p)

	def receive_blockchain(self, msg, sender):
		blockchain_array = msg[0]
		self.blockchain = blockchain_array

	def neighbor_exiting(self, msg, sender):
		for i in range(len(self.neighbors)):
			if self.neighbors[i].public_key == sender:
				del self.neighbors[i]
				break

		# contact new neighbors (ignore self and current neighbor nodes)
		new_peer_list = msg[0]
		for node in new_peer_list:
			if node.public_key == self.public_key:
				continue
			if node.public_key in [peer.public_key for peer in self.neighbors]:
				continue
			self.contact_new_peer(node)

	def handle_new_site(self, msg, sender):
		# either mine or send out to peers
		dir_update = msg[0]
		if is_mining == True:
			Thread(target=self.mine_WebsiteBlock, args=(dir_update)).start()
		else:
			self.announce_published_site(dir_update)




	'''
	Instructions from Main
	'''
	def instructions_handler(self, msg):
		msg_type = msg.pop(0)

		print("ID = {}\tpeers = {}".format(self.ID, self.neighbors))

		if msg_type == "individual_exit":
			self.individual_exit()
		elif msg_type == "hard_exit":
			self.hard_exit()
		elif msg_type == "publish":
			self.publish_website(msg)


	'''
	Helper Functions
	'''
	# create personalized directory to store web data
	def create_file_directory(self):
		self.folder_name = "./WebPages/node_" + str(self.ID)
		os.mkdir(self.folder_name)

	# def remove_file(self, url):
	# 	# delete website data from file directory

	# mine the blockchain
	def mine_WebsiteBlock(self, data):
		# while nlz != num
		# 	# get prev_block_hash & build block

		# 	# mine

		#	# make sure prev_block_hash is still relevent 

		# 	# make sure this block has not been mined already
		return

	# def verify_integrity(self):
	#	# veryify integrity of the blockchain

	# def remove_neighbor(self, msg):
	# 	# remove a peer from neighbor list
	# 	i = 0
	# 	for n in self.neighbors:
	# 		if n.ID = msg[1]:
	# 			self.neighbors.delete(i)
	# 			break
	# 		i += 1

	def individual_exit(self):
		msg = ["exiting_node", self.neighbors, datetime.utcnow().timestamp()]
		for peer in self.neighbors:
			self.network[peer.public_key].send_message(msg, self.public_key)

		shutil.rmtree(self.folder_name)

		self.continue_running = False

		# delete directory from filespace

	def hard_exit(self):
		shutil.rmtree(self.folder_name)
		self.continue_running = False

	# publish a website to network based on filename/location
	def publish_website(self, msg):
		filename = "./WebPages/Stored_WebPages/" + str(msg[0])
		new_location = self.folder_name + "/" + str(msg[0])
		publish_url = str(msg[0].split(".")[0]) + ".com"

		# find hash of file
		filehash = get_file_hash(filename)

		# copy file to node's own directory
		try:
			shutil.copyfile(filename, new_location)
			# os.rename(filename, new_location)
		except Exception as e:
			print("Error copying file: {}".format(e))

		# create directory update: website_id?
		dir_update = DirectoryUpdate(url=publish_url,
			files_hash=filehash,
			owner_pk=self.public_key,
			)

		# announce publishing of site
		self.announce_published_site(dir_update)

	def get_file_hash(self, filename):
		f=open(filename, "r")
		contents = f.read()
		return hashlib.sha256(contents.encode()).hexdigest()






#############################################################
# 							TEST							#
#############################################################
if __name__ == '__main__':

	network = {}	# all internode connection
	instruction_queues = {}
	seed_connections = []	# seed nodes
	threads = []	# threads

	seed_pub_key = "og_pub"
	seed_pvt_key = "og_pvt"
	seed_queue = NetworkQueue(seed_pvt_key)
	network[seed_pub_key] = seed_queue
	seed_connections.append(seed_pub_key)
	instructions = Queue()
	instruction_queues["0"] = instructions

	t = Node_Process(network, "0", seed_pub_key, seed_pvt_key, instructions)
	t.start()
	threads.append(t)

	for i in range(2):
		pub_key = "pub: " + str(i)
		pvt_key = "pvt: " + str(i)
		connection_queue = NetworkQueue(pvt_key)
		network[pub_key] = connection_queue

		ID = str(i+1)
		instructions = Queue()
		instruction_queues[ID] = instructions

		t = Node_Process(network, ID, pub_key, pvt_key, instructions, seed_pub_key)
		t.start()
		threads.append(t)

	# for q in range(3):
	# 	ID = str(q)
	# 	instruction_queues[ID].put(["exit", 4943932])

	for t in threads:
		t.join()