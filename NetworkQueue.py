#!/usr/bin/env python3

import hashlib
from queue import Queue

class NetworkQueue(Queue):

	def __init__(self, private_key):
		super().__init__()

		hash_str = private_key + " "
		self.signature = hashlib.sha256(hash_str.encode()).hexdigest()

	def send_message(self, msg, sender):
		# print("DEBUG: send_message()")
		self.put([msg, sender])
		# print("DEBUG: send_message() complete: {}".format(self.__myQueue))

	def receive_message(self, private_key):
		hash_str = private_key + " "
		testing_signature = hashlib.sha256(hash_str.encode()).hexdigest()
		if (self.signature == testing_signature):
			msg = self.get()
			return msg
		return None

# #############################################################
# # 							TEST							#
# #############################################################
# if __name__ == '__main__':

# 	net = NetworkQueue("private")
# 	net.send_message("hello there", "my public key")
# 	net.send_message("numba 2", "another key")
# 	print("empty = {}\tmsg = {}".format(net.empty(), net.receive_message("private")))
# 	print(net)
