import hashlib
import time

class Block:
    # def __repr__(self):
    #     return '<{0}: {1}>'.format(self.__class__.__name__, vars(self))

    # def __str__(self):
    #     return self.__repr__()

    def update_hash(self):
        concat = str(self.index) + str(self.timestamp) + str(self.data) + str(self.nonce) + str(self.previous_block_hash) + str(self.directory_hash)
        m = hashlib.sha256()
        m.update(concat.encode('utf-8'))
        self.hash = m.hexdigest()

    def mine(self, nlz=3):
        # self.nonce = "0"
        self.update_hash()
        while not self.hash[:nlz] == "0" * nlz:
            self.nonce = str(int(self.nonce) + 1)
            self.update_hash()
            time.sleep(0)