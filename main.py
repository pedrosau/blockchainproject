#!/usr/bin/env python3

import threading
import hashlib
import time
import datetime
from Node_threading import Node_Process
from Directory import DirectoryEntry, Directory
from Peer import Peer
from NetworkQueue import NetworkQueue
from queue import Queue
from TransactionBlock import TransactionBlock, Transaction, TranType
from random import shuffle

if __name__ == '__main__':

    # Declare Variables
    network = {}                # all inter-node connection
    instruction_queues = {}     # main-to-node communication
    node_keys = []              # keep track of nodes for seeds
    node_IDs = []
    threads = []                # all nodes run on separate threads

    while True:
        args = input('> ').strip().split()

        if args[0] == 'exit':
            print("\n\nDEBUG:\nnetwork = {}\n\ninstruction_queues = {}".format(network, instruction_queues))

            for ID in instruction_queues:
                instruction_queues[ID].put(["hard_exit"])

            for t in threads:
                t.join()

            # make sure all files end up in "Stored_WebPages" directory

            break

        # Usage: node create|delete node_id
        elif args[0] == 'node':
            node_id = None
            if len(args) == 3:
                if args[1] == 'create':
                    node_id = args[2]
                    if not node_id.isdigit():
                        print('Node ID must be a digit.')
                        continue
                    elif node_id in node_IDs:
                        print(f'Node with ID {node_id} already exists.')
                        continue
                    
                    # Create a new node
                    ID = str(node_id)

                    # This is to change
                    public_key = ID + "public"
                    public_key = hashlib.sha256(public_key.encode()).hexdigest()
                    private_key = ID + "private"
                    private_key = hashlib.sha256(private_key.encode()).hexdigest()

                    net_queue = NetworkQueue(private_key)
                    network[public_key] = net_queue
                    instructions = Queue()
                    instruction_queues[ID] = instructions

                    if len(node_keys) == 0:
                        t = Node_Process(network, ID, public_key, private_key, instructions)
                    else:
                        # shuffle(node_keys) # to be returned later
                        print("debug: node_keys[0] = {}".format(node_keys[0]))
                        t = Node_Process(network, ID, public_key, private_key, instructions, node_keys[0])

                    t.start()
                    threads.append(t)

                    node_keys.append(public_key)
                    node_IDs.append(ID)

                    print("DEBUG: node {} created".format(ID))

                elif args[1] == 'delete':
                    node_id = args[2]
                    if not node_id.isdigit():
                        print('Node ID must be a digit.')
                        continue
                    elif node_id not in node_IDs:
                        print(f'Node with ID {node_id} does not exist.')
                        continue
                    instruction_queues[node_id].put(["individual_exit"])


            if not node_id:
                print('Usage: node create|delete node_id')
                continue


        # Usage: publish -n owner_node_id -d website_directory
        elif args[0] == 'publish':
            owner_node_id = None
            directory = None

            for i in range(1, len(args)):
                if not len(args) == 5: break
                if args[i] == '-n': visitor_owner_node_idnode_id = args[i+1]
                elif args[i] == '-d': directory = args[i+1]

            if not owner_node_id or not directory:
                print('Usage: publish -n owner_node_id -d website_directory')
                continue
            elif not owner_node_id in nodes:
                print(f'Node with ID {owner_node_id} does not exist.')
                continue

            #TODO: Call node function to publish the site

        # Usage: > visit -n visitor_node_id -url url
        elif args[0] == 'visit':
            visitor_node_id = None
            url = None

            for i in range(1, len(args)):
                if not len(args) == 5: break
                if args[i] == '-n': visitor_node_id = args[i+1]
                elif args[i] == '-url': url = args[i+1]

            if not url or not visitor_node_id:
                print('Usage: visit -n visitor_node_id -url url')
                continue
            elif not visitor_node_id in nodes:
                print(f'Node with ID {visitor_node_id} does not exist.')
                continue

            #TODO: Call node function to visit the site