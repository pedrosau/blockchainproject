import datetime
from enum import Enum
from Block import Block

class TranType(Enum):
    VISIT = 0
    PUBLISH_NEW = 1
    PUBLISH_UPDATE = 2
    MINE_REWARD = 3
    VERIFY_REWARD = 4
    INITIALIZATION = 5


class Transaction:
    def __init__(self, sender="0", recipient="0", amount="0", type=TranType.VISIT):
        self.sender = str(sender).zfill(64)
        self.recipient = str(recipient).zfill(64)
        self.type = type
        self.amount = str(amount).zfill(64)
        self.timestamp = str(datetime.datetime.utcnow().timestamp())

    def __repr__(self):
        return 'TRANSACTION\nTYPE:{}\nSENDER:{}\nRECIPIENT:{}\nAMOUNT:{}\nTIMESTAMP:{}'.format(self.type.value, self.sender, self.recipient, self.amount, self.timestamp)

    def __str__(self):
        return self.__repr__()

class TransactionBlock(Block):
    def __init__(self, index="".zfill(64), timestamp="", data=Transaction(), nonce="0", previous_block_hash="", directory_hash=""):
        self.index = str(index).zfill(64)
        self.timestamp = str(timestamp)
        self.data = str(data)
        self.nonce = str(nonce)
        self.previous_block_hash = str(previous_block_hash)
        self.directory_hash = str(directory_hash)
        self.hash = ""

    def __repr__(self):
        return 'TRANSACTION BLOCK\nINDEX:{}\nNONCE:{}\nHASH:{}\nPREVIOUS BLOCK HASH:{}\nDIRECTORY HASH:{}\nTIMESTAMP:{}\n[{}]'.format(self.index, self.nonce, self.hash, self.previous_block_hash, self.directory_hash, self.timestamp, self.data)

    def __str__(self):
        return self.__repr__()