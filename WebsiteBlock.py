import hashlib
from datetime import datetime
from enum import Enum
from Block import Block

class DirectoryUpdateType(Enum):
	PUBLISH = 0
	UPDATE = 1

class DirectoryUpdate:
	def __init__(self, type=DirectoryUpdateType.PUBLISH, url="", files_hash="", owner_pk="", website_id=""):
		self.type = type
		self.url = str(url)
		self.files_hash = str(files_hash)
		self.owner_pk = str(owner_pk)
		self.website_id = str(website_id)
		self.timestamp = str(datetime.utcnow().timestamp())

	def __repr__(self):
		return f'WEBSITE\nTYPE:{self.type.value}\nURL:{self.url}\nFILES_HASH:{self.files_hash}\nOWNER_PK:{self.owner_pk}\nWEBSITE_ID:{self.website_id}\nTIMESTAMP:{self.timestamp}'

	def __str__(self):
		return self.__repr__()

class WebsiteBlock(Block):
	def __init__(self, index=''.zfill(64), timestamp=datetime.utcnow().timestamp(), data=DirectoryUpdate(), previous_block_hash='', directory_hash='', nonce=''):
		self.index = str(index).zfill(64)
		self.timestamp = str(timestamp)
		self.data = str(data)
		self.nonce = str(nonce)
		self.previous_block_hash = str(previous_block_hash)
		self.directory_hash = str(directory_hash)
		self.hash = ''

	def __repr__(self):
		return 'WEBSITE BLOCK\nINDEX:{}\nNONCE:{}\nHASH:{}\nPREVIOUS BLOCK HASH:{}\nDIRECTORY HASH:{}\nTIMESTAMP:{}\n[{}]'.format(self.index, self.nonce, self.hash, self.previous_block_hash, self.directory_hash, self.timestamp, self.data)

	def __str__(self):
		return self.__repr__()