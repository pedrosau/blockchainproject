#!/usr/bin/env python3

from multiprocessing import Process, Queue, Manager, Pool
from Directory import DirectoryEntry, Directory
from Peer import Peer
from NetworkQueue import NetworkQueue
import time
import datetime

import hashlib

class Node_Process(Process):

	def __init__(self, network, public_key, private_key, my_queue, initial_neighbors=[]):

		self.network = network
		print("\nnetwork:\n{}\n".format(network))

		self.public_key = public_key
		self.private_key = private_key
		self.my_queue = my_queue

		self.continue_running = True

		self.neighbors = []
		self.directory = Directory()
		self.hosted_sites = []

		for peer_key in initial_neighbors:
			p = Peer(peer_key)
			self.neighbors.append(p)
			self.contact_new_peer(p)

		# # Create a file directory for web data
		# # create_file_directory()

		self.handler()

	# Handle requests in the queue
	def handler(self):

		i = 0

		while self.continue_running == True:

			if i > 2:
				self.continue_running = False
			i += 1

			time.sleep(1)

			# try:

			print("\tdebug: empty(): {}: {}".format(self.public_key, self.my_queue.empty()))
			print("\tdebug: empty(): {}: {}".format(self.public_key, self.network[self.public_key].empty()))

			if self.my_queue.empty() == False:				
				print("\t\tdebug: receive if statement")
				msg = self.my_queue.receive_message()	# msg will always be a list
			else:
				print("{}: empty queue\tcontinue_running = {}".format(self.public_key, self.continue_running))
				continue

			# except Exception as e:
			# 	print("error: {}".format(e))
			# 	time.sleep(1)
			# 	continue

			print("name: {},\tmsg received: {}".format(self.ID, msg))

			msg_type = msg[0]
			msg = msg[1:]

			if msg_type == "contact_request":
				self.new_peer_request(msg)
			# elif msg_type == "exiting_peer":
			# 	self.remove_neighbor(msg)
			# elif msg_type == "maintain_connection":
			# 	self.return_maintain_connection(msg)
			elif msg_type == "exit":
				self.exit()


			# except Exception as e:
			# 	print("error: {}".format(e))
			# 	time.sleep(1)

	# Initial contact to a future neighbor
	def contact_new_peer(self, new_peer):
		# message format: [message type, my_queue, my_ID, timestamp]

		msg = ["contact_request", datetime.datetime.utcnow().timestamp()]
		print("contact_new_peer: {}".format(msg))
		try:
			self.network[new_peer.public_key].send_message(msg, self.public_key)
		except Exception as e:
			print("network_connections did not work: \t{}".format(e))
		print("debug: post send_message()")

	# an unknown node wants to become neighbors
	def new_peer_request(self, msg):
		# incoming message format: [peer's queue, peer's ID, timestamp]
		# p = Peer(msg[1], msg[2])
		# self.neighbors.append(p)

		# outgoing message format: [message type, my_ID, timestamp]
		response = ["confirm_neighbor", self.ID, datetime.datetime.utcnow().timestamp()]
		p.queue.put(response)

	# last step in establishing neighbor relationship
	# def confirm_neighbor(self, msg):
	# 	# incoming message format: [peer's ID, timestamp]


	# def create_file_directory(self):
	# 	# create personalized directory to store web data

	# def initial_block_download(self):
	# 	# download blockchain and directory from peer

	# def get_block(self):
	# 	# get a block from neighbor

	# def request_site(self, url):
	# 	# request website data from peers (function called from main)

	# def publish_website(self, filename):
	# 	# publish a website to network based on filename/location

	# def remove_file(self, url):
	# 	# delete website data from file directory

	# def mine(self):
	# 	# mine the blockchain

	# def broadcast_block(self):
	# 	# broadcast mined block to neighbors

	# def maintain_connection(self, peer):
	# 	# send a message to silent neighbors
	#	msg = ["maintain_connection", self.ID, timestamp]
	#	peer.queue.put(msg)

	# def return_maintain_connection(self,msg):
	#	# find peer by ID
	#	msg = ["connected"]
	#	peer.queue.put[msg]

	# def remove_neighbor(self, msg):
	# 	# remove a peer from neighbor list
	# 	i = 0
	# 	for n in self.neighbors:
	# 		if n.ID = msg[1]:
	# 			self.neighbors.delete(i)
	# 			break
	# 		i += 1


	def exit(self):
		# allow node to safely exit the network & connect peers
		self.continue_running = False
		# delete directory from filespace

#############################################################
# 							TEST							#
#############################################################
if __name__ == '__main__':

	pool = Pool()
	m = Manager()

	running_processes = []

	# seed_q = m.Queue()
	# running_queues.append(seed_q)
	# pool.apply_async(Node_Process, ('seed', seed_q))
	# # seed_proc = Process(target=Node_Process, args=('seed', seed_q))
	# # seed_proc.start()

	# i = 1
	# n = 6
	# for node in range(i,n+1):
	# 	p_queue = m.Queue()
	# 	running_queues.append(p_queue)
	# 	ID = "p" + str(node)
	# 	# p1 = Process(target=Node_Process, args=(ID, p_queue, [seed_q]))
	# 	# p1.start()
	# 	pool.apply_async(Node_Process, (ID, p_queue, [seed_q]))
	# 	break

	# time.sleep(5)

	# for node in running_queues:
	# 	node.put(["exit"])

	# pool.close()
	# pool.join()

	network_connections = m.dict()
	seed_connections = []

	seed_pub_key = "og_pub"
	seed_pvt_key = "og_pvt"
	seed_queue = NetworkQueue(seed_pvt_key)

	seed_process = Process(target=Node_Process, args=(network_connections, seed_pub_key, seed_pvt_key, seed_queue))
	seed_process.start()
	running_processes.append(seed_process)

	# pool.apply_async(Node_Process, (network_connections, seed_pub_key, seed_pvt_key, seed_queue))

	seed_connections.append(seed_pub_key)
	network_connections[seed_pub_key] = seed_queue

	for i in range(2):
		pub_key = "pub: " + str(i)
		pvt_key = "pvt: " + str(i)
		connection_queue = NetworkQueue(pvt_key)
		network_connections[pub_key] = connection_queue
		p = Process(target=Node_Process, args=(network_connections, pub_key, pvt_key, connection_queue, [seed_pub_key]))
		p.start()
		running_processes.append(p)
		# pool.apply_async(Node_Process, (network_connections, pub_key, pvt_key, connection_queue, [seed_pub_key]))

	# pool.close()
	# pool.join()
	for p in running_processes:
		p.join()



