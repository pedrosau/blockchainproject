from WebsiteBlock import DirectoryUpdate, WebsiteBlock, DirectoryUpdateType
from collections.abc import MutableMapping
import hashlib

class DirectoryEntry:
    def __init__(self, files_hash, owner_pk, website_id):
        self.files_hash = str(files_hash)
        self.owner_pk = str(owner_pk)
        self.website_id = str(website_id)
    def __repr__(self):
        return f"<DirectoryEntry: [FilesHash: {self.files_hash}, OwnerPK: {self.owner_pk}, WebsiteID: {self.website_id}]>"

class Directory(MutableMapping):
    '''def __init__(self, *args, **kwargs):
        self.__dict__.update(*args, **kwargs)'''

    def __init__(self, blockchain=[]):
        for block in blockchain:
            if not block.data.split('\n')[0] == 'WEBSITE': continue

            data = {entry.split(':')[0]: entry.split(':')[1] for entry in block.data.split('\n')[1:]}
            if int(data['TYPE']) == DirectoryUpdateType.PUBLISH.value:
                if not data['URL'] in self.__dict__:
                    self[data['URL']] = DirectoryEntry(data['FILES_HASH'], data['OWNER_PK'], data['WEBSITE_ID'])
            elif int(data['TYPE']) == DirectoryUpdateType.UPDATE.value:
                if data['URL'] in self.__dict__:
                    self[data['URL']].files_hash =  data['FILES_HASH']

    def __setitem__(self, key, value):
        if isinstance(value, DirectoryEntry):
            self.__dict__[key] = value

    def __getitem__(self, key):
        return self.__dict__[key]

    def __delitem__(self, key):
        del self.__dict__[key]

    def __iter__(self):
        return iter(self.__dict__)

    def __len__(self):
        return len(self.__dict__)

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return self.__str__()

    def items(self):
        return self.__dict__.items()

    def hash(self):
        concat = ''.join(str(entry) for entry in sorted(self.__dict__.items()))
        m = hashlib.sha256()
        m.update(concat.encode('utf-8'))
        return m.hexdigest()
        
if __name__ == '__main__':
    block1 = WebsiteBlock(index=1, data=DirectoryUpdate(url='google.com', files_hash='abc123'))
    block2 = WebsiteBlock(index=2, data=DirectoryUpdate(url='yahoo.com', files_hash='123abc'))
    block3 = WebsiteBlock(index=3, data=DirectoryUpdate(url='facebook.com', files_hash='xyz321'))
    block4 = WebsiteBlock(index=4, data=DirectoryUpdate(url='google.com', files_hash='321xyz', type=DirectoryUpdateType.UPDATE))
    blockchain = [block1, block2, block3, block4]
    d = Directory(blockchain=blockchain)
    print(d)